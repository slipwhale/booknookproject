import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  ImageBackground,
  Image,
} from "react-native";

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      password: "",
      confirmPassword: "",
      fullName: "", // New state property for full name
      errorMessage: "",
    };
  }

  handleSignUp = () => {
    const { username, email, password, confirmPassword, fullName } = this.state;

    if (!username || !email || !password || !confirmPassword || !fullName) {
      this.setState({ errorMessage: "Please fill out all fields" });
      return;
    }

    if (password !== confirmPassword) {
      this.setState({ errorMessage: "Passwords don't match" });
    } else {
      this.setState({ errorMessage: "" });
      this.props.navigation.navigate("Profile", { username, email, fullName });
    }
  };

  render() {
    return (
      <ImageBackground
        source={require("./background.png")}
        style={styles.backgroundImage}
      >
        <View style={styles.container}>
          <Image source={require("./Logo.png")} style={styles.logo} />
          <Text>Sign Up</Text>
          <TextInput
            style={styles.input}
            placeholder="Username"
            onChangeText={(text) => this.setState({ username: text })}
          />
          <TextInput
            style={styles.input}
            placeholder="Email"
            onChangeText={(text) => this.setState({ email: text })}
          />
          <TextInput
            style={styles.input}
            placeholder="Full Name" // New input field for full name
            onChangeText={(text) => this.setState({ fullName: text })}
          />
          <TextInput
            style={styles.input}
            placeholder="Password"
            secureTextEntry={true}
            onChangeText={(text) => this.setState({ password: text })}
          />
          <TextInput
            style={styles.input}
            placeholder="Confirm Password"
            secureTextEntry={true}
            onChangeText={(text) => this.setState({ confirmPassword: text })}
          />
          {this.state.errorMessage ? (
            <Text style={styles.errorMessage}>{this.state.errorMessage}</Text>
          ) : null}
          <Button title="Sign Up" onPress={this.handleSignUp} />
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: "cover",
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  logo: {
    width: 120,
    height: 120,
    marginBottom: 0,
  },
  input: {
    width: "80%",
    padding: 10,
    margin: 5,
    borderRadius: 10,
    borderColor: "gray",
    borderWidth: 1,
    backgroundColor: "rgba(255, 255, 255, 0.8)",
  },
  errorMessage: {
    color: "red",
    marginBottom: 10,
  },
  button: {
    width: "80%",
  },
});