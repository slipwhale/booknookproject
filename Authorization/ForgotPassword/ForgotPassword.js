import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, Image, ImageBackground } from 'react-native';
import InputField from '../../components/InputField'; 

export default function ForgotPassword({ navigation }) {
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [emailSent, setEmailSent] = useState(false);

  const handleSendEmail = () => {
    

    setMessage(`Password reset instructions sent to ${email}`);
    setEmailSent(true);
  };

  return (
    <ImageBackground source={require('./background.png')} style={styles.backgroundImage}>
      <View style={styles.container}>
        <Image source={require('./Logo.png')} style={styles.logo} />
        <Text style={styles.loginText}>Forgot Password</Text>
        {emailSent ? (
          <Text style={styles.message}>{message}</Text>
        ) : (
          <>
            <Text style={styles.label}>Enter your email address:</Text>
            <TextInput
              style={styles.input}
              placeholder="Email"
              value={email}
              onChangeText={(text) => setEmail(text)}
            />
            <Button title="Send Reset Email" onPress={handleSendEmail} color="#FF69B4" />
          </>
        )}
        <Button title="Back to Login" onPress={() => navigation.navigate('Login')} color="#FF69B4" />
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
  loginText: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: '#FF69B4', 
  },
  label: {
    fontSize: 18,
    marginBottom: 10,
  },
  input: {
    width: '100%',
    height: 40,
    borderWidth: 1,
    borderColor: '#FF69B4', 
    borderRadius: 5,
    marginBottom: 10,
    paddingLeft: 10,
  },
  message: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 20,
    color: 'blue',
  },
});
