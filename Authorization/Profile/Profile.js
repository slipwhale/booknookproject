import React from "react";
import { View, Text, Image, ImageBackground, StyleSheet, TouchableWithoutFeedback } from "react-native";

export default function Profile({ route, navigation }) {
  const { username, email, fullName } = route.params;

  return (
    <ImageBackground
      source={require("./background.png")}
      style={styles.backgroundImage}
    >
      <View style={styles.container}>
        {/* Header: User's Profile */}
        <Text style={styles.header}>User's Profile</Text>

        {/* Photo */}
        <Image
          source={require("./userPhoto.png")}
          style={styles.userPhoto}
        />

        {/* User Data */}
        <View style={styles.profileContainer}>
          <View style={styles.dataContainer}>
            <View style={styles.dataRow}>
              <Text style={styles.label}>Username:</Text>
              <Text style={styles.value}>{username}</Text>
            </View>
            <View style={styles.dataRow}>
              <Text style={styles.label}>Email:</Text>
              <Text style={styles.value}>{email}</Text>
            </View>
            <View style={styles.dataRow}>
              <Text style={styles.label}>Full Name:</Text>
              <Text style={styles.value}>{fullName}</Text>
            </View>
          </View>
        </View>
      </View>

      {/* Footer: Navigation Links */}
      <View style={styles.footer}>
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate("Purchased")}
        >
          <Text style={styles.footerLink}>Purchased</Text>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate("Wishlists")}
        >
          <Text style={styles.footerLink}>Wishlists</Text>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate("Activity")}
        >
          <Text style={styles.footerLink}>Activity</Text>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate("Dashboard")}
        >
          <Text style={styles.footerLink}>Go to Dashboard</Text>
        </TouchableWithoutFeedback>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: "cover",
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  header: {
    fontSize: 24,
    fontWeight: "bold",
    textAlign: "center",
    marginBottom: 20,
  },
  userPhoto: {
    width: 150,
    height: 150,
    borderRadius: 75,
  },
  profileContainer: {
    backgroundColor: "rgba(255, 255, 255, 0.8)",
    padding: 20,
    borderRadius: 10,
    marginTop: 20,
    borderWidth: 2,
    borderColor: "#007BFF",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
  dataContainer: {
    marginVertical: 10,
  },
  dataRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  label: {
    fontSize: 18,
    fontWeight: "bold",
  },
  value: {
    fontSize: 16,
  },
  footer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 20,
    paddingHorizontal: 20,
  },
  footerLink: {
    fontSize: 16,
    color: "#007BFF",
    textDecorationLine: "underline",
  },
});