import React, { useState } from 'react';
import { View, Text, StyleSheet, ImageBackground, Image, ScrollView, Alert } from 'react-native';
import InputField from '../../components/InputField';
import ThemeButton from '../../components/ThemeButton';

export default function Login({ navigation }) {
  const [data, setData] = useState({
    username: '',
    password: '',
  });

  const handleLogin = () => {
    if (data.username.trim() === '' || data.password.trim() === '') {
      Alert.alert('Empty Fields', 'Please fill in both username and password fields.');
      return;
    }

    navigation.navigate('Profile', data); // Proceed to the "Profile" page with user data
  };

  return (
    <ImageBackground source={require('./background.png')} style={styles.backgroundImage}>
      <ScrollView contentContainerStyle={styles.container}>
        <Image source={require('./Logo.png')} style={styles.logo} />
        <Text style={styles.loginText}>Login</Text>
        <InputField
          placeholder="Username"
          value={data.username}
          onChangeText={(value) => setData({ ...data, username: value })}
        />
        <InputField
          secureTextEntry={true}
          placeholder="Password"
          value={data.password}
          onChangeText={(value) => setData({ ...data, password: value })}
        />
        <ThemeButton
          title="Login"
          color="#FF69B4"
          onPress={handleLogin}
          buttonStyle={styles.button}
        />
        <Text style={styles.forgotPassword} onPress={() => navigation.navigate('ForgotPassword')}>
          Forgot your password?
        </Text>
        <Text style={styles.signUpLink} onPress={() => navigation.navigate('SignUp')}>
          Don't have an account? Sign Up
        </Text>
      </ScrollView>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 30, 
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 20, 
  },
  loginText: {
    fontSize: 24, 
    marginBottom: 20,
    fontWeight: 'bold',
  },
  button: {
    width: '40%', 
    height: 40, 
    marginBottom: 10,
  },
  forgotPassword: {
    color: '#007BFF', 
    fontSize: 16,
    marginVertical: 10,
  },
  signUpLink: {
    color: '#007BFF', 
    fontSize: 16,
  },
});