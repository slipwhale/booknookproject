import React from "react";
import { TextInput, StyleSheet, View } from "react-native";

export default function InputField(props) {
  const {
    placeholder,
    value,
    onChangeText,
    secureTextEntry,
    containerStyle,
    inputStyle,
    ...otherProps
  } = props;

  return (
    <View style={[styles.container, containerStyle]}>
      <TextInput
        style={[styles.inputField, inputStyle]}
        placeholder={placeholder}
        value={value}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
        {...otherProps}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  inputField: {
    width: 200, 
    height: 40, 
    borderWidth: 1,
    borderRadius: 10,
    padding: 12,
    borderColor: "#ccc",
    backgroundColor: "#fff",
  },
});
