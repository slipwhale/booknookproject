import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

export default function ThemeButton(props) {
  return (
    <TouchableOpacity
      style={[styles.button, props.buttonStyle]}
      onPress={props.onPress}
    >
      <Text style={styles.buttonText}>{props.title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'pink',
    height: 50, 
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10, 
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
  },
});