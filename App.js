import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import Login from './Authorization/Login/Login';
import Profile from './Authorization/Profile/Profile';
import SignUp from './Authorization/SignUp/SignUp';
import ForgotPassword from './Authorization/ForgotPassword/ForgotPassword';



const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name='Login' component={Login} />
        <Stack.Screen name='Profile' component={Profile} />
        <Stack.Screen name='SignUp' component={SignUp} />
        <Stack.Screen name='ForgotPassword' component={ForgotPassword} />
      </Stack.Navigator>
    </NavigationContainer>
  );}